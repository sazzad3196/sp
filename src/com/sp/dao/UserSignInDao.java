package com.sp.dao;

import java.sql.ResultSet;
import java.sql.Statement;

import com.sp.servlet.UserData;

public class UserSignInDao {
	
	Database database;
	public UserSignInDao(Database db) {
		this.database = db;
	}
	
	public UserData userAuthorization(String email, String password) {
		Statement statement = null;
		ResultSet RS = null;
		String sql = null;
		UserData userData = null;
		
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from userinfo where email='" + email + "' and password='" + password + "'";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				userData = new UserData();
				userData.setId(Integer.parseInt(RS.getString(1)));
				userData.setName(RS.getString(2));
				userData.setEmail(RS.getString(3));
				userData.setPassword(RS.getString(4));
				userData.setPhoneNo(RS.getString(5));
				userData.setGender(RS.getString(6));
				userData.setAge(Integer.parseInt(RS.getString(7)));
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return userData;
	}
}
