package com.sp.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.tomcat.util.http.fileupload.FileItemStream;

public class UploadImageDao {
	Database database;
	
	public UploadImageDao(Database db) {
		database = db;
	}
	
	public boolean processFile(String path , FileItemStream item , String img) {
		  String image,part1,part2;
		  
		  try {
			  File f = new File(path + File.separator + "images");
			  if(!f.exists()) {
				  f.mkdirs();
			  }
		
			  File saveFile = new File(f.getAbsolutePath() + File.separator + img);
			  FileOutputStream os = new FileOutputStream(saveFile);
			  InputStream is = item.openStream();
			  int x = 0;
			  byte[] b = new byte[1024];
			  while((x=is.read(b)) != -1) {
					os.write(b, 0, x);
			  }
			  os.flush();
			  os.close();
			  return true;
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
		}
		return false;
	 }

}
