package com.sp.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {
	private String databaseDriver = null;
	private String databaseName = null;
	private String userName = null;
	private String password = null;
	private Connection connection = null;
	
	public Database(String databaseDriver , String databaseName , String userName , String password) {
		 this.databaseDriver = databaseDriver;
		 this.databaseName = databaseName;
		 this.userName = userName;
		 this.password = password;
	}
	
	public void open() {
		try {
			 Class.forName(databaseDriver);
		     connection = DriverManager.getConnection(databaseName,userName,password);
		}
		catch (Exception e) {
			System.out.println("Not Connect!!");
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			if(connection != null) connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}

}
