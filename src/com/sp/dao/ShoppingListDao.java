package com.sp.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sp.servlet.CardData;
import com.sp.servlet.OrderData;
import com.sp.servlet.ShoppingListData;
import com.sp.servlet.UserData;

public class ShoppingListDao {
	Database database;
	
	public ShoppingListDao(Database db) {
		this.database = db;
	}
	
	public List<ShoppingListData> getAllBread(String item) {
	    Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		List<ShoppingListData> ul = new ArrayList<ShoppingListData>();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from iteminfo where catagory= '" + item + "'";
			RS = statement.executeQuery(sql);
			while (RS.next()) {
				ShoppingListData breadData = new ShoppingListData();
				breadData.setId(Integer.parseInt(RS.getString(1)));
				breadData.setName(RS.getString(2));
				breadData.setPrice(Integer.parseInt(RS.getString(3)));
				breadData.setCatagory(RS.getString(4));
				breadData.setImage(RS.getString(5));
				ul.add(breadData);
			}
		}catch (Exception e) {
			System.out.println("Error!!!!");
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
		
	}
	
	public List<ShoppingListData> getAllFruits(String item) {
	    Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		List<ShoppingListData> ul = new ArrayList<ShoppingListData>();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from iteminfo where catagory= '" + item + "'";
			RS = statement.executeQuery(sql);
			while (RS.next()) {
				ShoppingListData fruitsData = new ShoppingListData();
				fruitsData.setId(Integer.parseInt(RS.getString(1)));
				fruitsData.setName(RS.getString(2));
				fruitsData.setPrice(Integer.parseInt(RS.getString(3)));
				fruitsData.setCatagory(RS.getString(4));
				fruitsData.setImage(RS.getString(5));
				ul.add(fruitsData);
			}
		}catch (Exception e) {
			System.out.println("Error!!!!");
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
		
	}
	
	public List<ShoppingListData> getAllVegetable(String item) {
	    Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		List<ShoppingListData> ul = new ArrayList<ShoppingListData>();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from iteminfo where catagory= '" + item + "'";
			RS = statement.executeQuery(sql);
			while (RS.next()) {
				ShoppingListData vegetableData = new ShoppingListData();
				vegetableData.setId(Integer.parseInt(RS.getString(1)));
				vegetableData.setName(RS.getString(2));
				vegetableData.setPrice(Integer.parseInt(RS.getString(3)));
				vegetableData.setCatagory(RS.getString(4));
				vegetableData.setImage(RS.getString(5));
				ul.add(vegetableData);
			}
		}catch (Exception e) {
			System.out.println("Error!!!!");
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
		
	}
	
	public List<ShoppingListData> getAllDairy(String item) {
	    Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		List<ShoppingListData> ul = new ArrayList<ShoppingListData>();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from iteminfo where catagory= '" + item + "'";
			RS = statement.executeQuery(sql);
			while (RS.next()) {
				ShoppingListData dairyData = new ShoppingListData();
				dairyData.setId(Integer.parseInt(RS.getString(1)));
				dairyData.setName(RS.getString(2));
				dairyData.setPrice(Integer.parseInt(RS.getString(3)));
				dairyData.setCatagory(RS.getString(4));
				dairyData.setImage(RS.getString(5));
				ul.add(dairyData);
			}
		}catch (Exception e) {
			System.out.println("Error!!!!");
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
		
	}
	
	 public boolean addShoppingList(String name, String catagory, String imageName, int price) {
		 Statement statement = null;
		 String sql = null;
		 try {
			  statement = database.getConnection().createStatement();
			  sql = " insert into iteminfo (name,image,catagory,price) values('" + name + "','" + imageName + "','" + catagory + "'," + price + ")";
			  statement.executeUpdate(sql);
		 }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
			  return false;
		}finally {
			 try {
				 if(statement != null) statement.close();
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
			 }
		}
		 
		return true; 
		 
	 }
	 
	 public boolean editShoppingList(int id, String name, String catagory, String imageName, int price) {
		 Statement statement = null;
		 String sql = null;
		 try {
			  statement = database.getConnection().createStatement();
			  sql = " update iteminfo set name='" + name + "',catagory='" + catagory + "',image='" + imageName + "',price=" + price + " where id=" + id;
			  statement.executeUpdate(sql);
		 }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
			  return false;
		}finally {
			 try {
				 if(statement != null) statement.close();
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
			 }
		}
		 
		return true; 
		 
	 }
	 
	 public List<ShoppingListData> getAllProduct(String item) {
		    Statement statement = null;
			String sql = null;
			ResultSet RS = null;
			List<ShoppingListData> ul = new ArrayList<ShoppingListData>();
			try {
				statement = database.getConnection().createStatement();
				sql = "select * from iteminfo";
				RS = statement.executeQuery(sql);
				while (RS.next()) {
					ShoppingListData breadData = new ShoppingListData();
					breadData.setId(Integer.parseInt(RS.getString(1)));
					breadData.setName(RS.getString(2));
					breadData.setPrice(Integer.parseInt(RS.getString(3)));
					breadData.setCatagory(RS.getString(4));
					breadData.setImage(RS.getString(5));
					ul.add(breadData);
				}
			}catch (Exception e) {
				System.out.println("Error!!!!");
				// TODO: handle exception
				e.printStackTrace();
			}finally {
				try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			}
			
			return ul;
			
		}
	 
	 
	 public boolean deleteProduct(int id) {
		 Statement statement = null;
		 String sql = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "delete from iteminfo where id=" + id;
			 statement.executeUpdate(sql);
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		 return true;
	 }
	 
	 public boolean deleteCardInfo(int id) {
		 Statement statement = null;
		 String sql = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "delete from cardinfo where id=" + id;
			 statement.executeUpdate(sql);
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		 return true;
	 }
	 
	 
	 public boolean deleteOrderInfo(int id) {
		 int cardId = 0;
		 boolean value;
		 List<OrderData> ul = getOrderInfo();
		 for(OrderData orderData: ul) {
			 if(orderData.getId() == id) {
				 cardId = orderData.getCardId();
			 }
		 }
		 value = deleteCardInfo(cardId);
		 if( value == true) {
			 Statement statement = null;
			 String sql = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = "delete from orderinfo where id=" + id;
				 statement.executeUpdate(sql);
				 
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
				 return false;
			}finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		 }
		 return true;
	 }
	 
	 
	 public CardData getOneCardInfo(int userId, int itemId) {
		 CardData cardData = null;
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from cardinfo where userId=" + userId + " and itemId=" + itemId + " and status=" + 1;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 cardData = new CardData();
				 cardData.setId(Integer.parseInt(RS.getString(1)));
				 cardData.setUserId(Integer.parseInt(RS.getString(3)));
				 cardData.setItemId(Integer.parseInt(RS.getString(2)));
				 cardData.setQuantity(Integer.parseInt(RS.getString(4)));
			 }
			 
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		 
		return cardData;
	 }
	 
	 public boolean addCardInfo(int userId, int itemId, int quantity) {
		  CardData cardData = getOneCardInfo(userId, itemId);
		  Statement statement = null;
		  String sql = null;
		  try {
			 statement = database.getConnection().createStatement();
			 if (cardData == null) { 
			     sql = "insert into cardinfo(userId,itemId,quantity,status) values(" + userId + "," + itemId + "," + quantity + ",1)";
			 }
			 else {
				 int quan = quantity + cardData.getQuantity();
				 sql = "update cardinfo set quantity=" + quan + " where id=" + cardData.getId();
			 }
			 statement.executeUpdate(sql);
		}catch(Exception e) {
				// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return true;
	 }
	 
	 
	 public List<CardData> getCardInfo(int userId, int status) {
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 List<CardData> ul = new ArrayList<CardData>();
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from cardinfo where userId=" + userId + " and status=" + status;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 int itemId;
				 CardData cardData = new CardData();
				 cardData.setId(Integer.parseInt(RS.getString(1)));
				 cardData.setItemId(Integer.parseInt(RS.getString(2)));
				 cardData.setUserId(Integer.parseInt(RS.getString(3)));
				 cardData.setQuantity(Integer.parseInt(RS.getString(4)));
				 itemId = Integer.parseInt(RS.getString(2));
				 Statement statement1 = null;
				 String sql1 = null;
				 ResultSet RS1 = null;
				 ShoppingListData shoppingListData = new ShoppingListData();
				 try {
					 statement1 = database.getConnection().createStatement();
					 sql1 = "select * from iteminfo where id=" + itemId;
					 RS1 = statement1.executeQuery(sql1);
					 while(RS1.next()) {
						 shoppingListData.setId(itemId);
						 shoppingListData.setName(RS1.getString(2));
						 shoppingListData.setPrice(Integer.parseInt(RS1.getString(3)));
						 shoppingListData.setCatagory(RS1.getString(4));
						 shoppingListData.setImage(RS1.getString(5));
					 }
				 }catch (Exception e) {
					// TODO: handle exception
					 System.out.println("Error!!: ");
					 e.printStackTrace();
				}finally {
					try {
						if(RS1 != null) RS1.close();
						}catch (Exception e) {
							// TODO: handle exception
						    e.printStackTrace();
					    }
				    }
				    try {
						if(statement1 != null) statement1.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				 cardData.setShoppingListData(shoppingListData);
				 ul.add(cardData);
			 }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		 return ul;
		 
	 }
	 
	 
	 
	 public void updateCardInfo(int userId, int cardId) {
		 Statement statement = null;
		 String sql = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "update cardinfo set status=2 where userId=" + userId + " and status=" + 1 + " and id=" + cardId; 
			 statement.executeUpdate(sql);
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			 try {
				 if(statement != null) statement.close();
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
			 }
		}
	 }
	 
	 public boolean setOrderInfo(int cardId, int userId, String address, String city, String date) {
		  updateCardInfo(userId, cardId);
		  Statement statement = null;
		  String sql = null;
		  try {
			  statement = database.getConnection().createStatement();
			  sql = " insert into orderinfo(cardId,userId,address,city,date) values(" + cardId + "," + userId + ",'" + address + "','" + city + "','" + date + "')";
			  statement.executeUpdate(sql);
		}catch(Exception e) {
				// TODO: handle exception
			 e.printStackTrace();
			 return false;
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return true;
	}
	 
	 public List<OrderData> getOrderInfo() {
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 List<OrderData> ul = new ArrayList<OrderData>();
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from orderinfo";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 OrderData orderData = new OrderData();
				 orderData.setId(Integer.parseInt(RS.getString(1)));
				 orderData.setUserId(Integer.parseInt(RS.getString(2)));
				 orderData.setAddress(RS.getString(3));
				 orderData.setCity(RS.getString(4));
				 orderData.setDate(RS.getString(5));
				 orderData.setCardId(Integer.parseInt(RS.getString(6)));
				 ul.add(orderData);
			 }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		 
		return ul;
	 }
	 
	 public List<OrderData> getAllOrderInformation() {
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 List<OrderData> ul = new ArrayList<OrderData>();
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from orderinfo";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 OrderData orderData = new OrderData();
				 int userId = Integer.parseInt(RS.getString(2));
				 int cardId = Integer.parseInt(RS.getString(6));
				 orderData.setId(Integer.parseInt(RS.getString(1)));
				 orderData.setUserId(Integer.parseInt(RS.getString(2)));
				 orderData.setAddress(RS.getString(3));
				 orderData.setCity(RS.getString(4));
				 orderData.setDate(RS.getString(5));
				 orderData.setCardId(Integer.parseInt(RS.getString(6)));
				 Statement statement1 = null;
				 String sql1 = null;
				 ResultSet RS1 = null;
				 UserData userData = new UserData();
				 try {
					 statement1 = database.getConnection().createStatement();
					 sql1 = "select * from userinfo where id=" + userId;
					 RS1 = statement1.executeQuery(sql1);
					 while(RS1.next()) {
						userData.setId(Integer.parseInt(RS1.getString(1)));
						userData.setName(RS1.getString(2));
						userData.setEmail(RS1.getString(3));
						userData.setPassword(RS1.getString(4));
						userData.setPhoneNo(RS1.getString(5));
						userData.setGender(RS1.getString(6));
						userData.setAge(Integer.parseInt(RS1.getString(7)));
					 }
				 }catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				 }finally {
						try {
							if(RS1 != null) RS1.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							try {
								if(statement1 != null) statement1.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
				}
				 
				 orderData.setUserData(userData);
				 Statement statement2 = null;
				 String sql2 = null;
				 ResultSet RS2 = null;
				 CardData cardData = new CardData();
				 try {
					 statement2 = database.getConnection().createStatement();
					 sql2 = "select * from cardinfo where id=" + cardId + " and status=" + 2;
					 RS2 = statement2.executeQuery(sql2);
					 while(RS2.next()) {
						 int itemId;
						 cardData.setId(Integer.parseInt(RS2.getString(1)));
						 cardData.setItemId(Integer.parseInt(RS2.getString(2)));
						 cardData.setUserId(Integer.parseInt(RS2.getString(3)));
						 cardData.setQuantity(Integer.parseInt(RS2.getString(4)));
						 itemId = Integer.parseInt(RS2.getString(2));
						 Statement statement3 = null;
						 String sql3 = null;
						 ResultSet RS3 = null;
						 ShoppingListData shoppingListData = new ShoppingListData();
						 try {
							 statement3 = database.getConnection().createStatement();
							 sql3 = "select * from iteminfo where id=" + itemId;
							 RS3 = statement3.executeQuery(sql3);
							 while(RS3.next()) {
								 shoppingListData.setId(itemId);
								 shoppingListData.setName(RS3.getString(2));
								 shoppingListData.setPrice(Integer.parseInt(RS3.getString(3)));
								 shoppingListData.setCatagory(RS3.getString(4));
								 shoppingListData.setImage(RS3.getString(5));
							 }
						 }catch (Exception e) {
							 e.printStackTrace();
						}finally {
							try {
								if(RS3 != null) RS3.close();
								}catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}
								try {
									if(statement3 != null) statement3.close();
								}catch (Exception e) {
									e.printStackTrace();
								}
					    }
						cardData.setShoppingListData(shoppingListData); 
					 }
					 
				 }catch (Exception e) {
					e.printStackTrace();
				}finally {
					try {
						if(RS2 != null) RS2.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement2 != null) statement2.close();
						}catch (Exception e) {
							e.printStackTrace();
						}
			    }
				orderData.setCardData(cardData); 
				ul.add(orderData);
			 }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		 
		return ul;
	 }
	 
	 
	 
	 public CardData getSingleCardInfo(int cardId, int status) {
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 CardData cardData = new CardData();
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from cardinfo where id=" + cardId + " and status=" + status;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 int itemId;
				 cardData.setId(Integer.parseInt(RS.getString(1)));
				 cardData.setItemId(Integer.parseInt(RS.getString(2)));
				 cardData.setUserId(Integer.parseInt(RS.getString(3)));
				 cardData.setQuantity(Integer.parseInt(RS.getString(4)));
				 itemId = Integer.parseInt(RS.getString(2));
				 Statement statement1 = null;
				 String sql1 = null;
				 ResultSet RS1 = null;
				 ShoppingListData shoppingListData = new ShoppingListData();
				 try {
					 statement1 = database.getConnection().createStatement();
					 sql1 = "select * from iteminfo where id=" + itemId;
					 RS1 = statement1.executeQuery(sql1);
					 while(RS1.next()) {
						 shoppingListData.setId(itemId);
						 shoppingListData.setName(RS1.getString(2));
						 shoppingListData.setPrice(Integer.parseInt(RS1.getString(3)));
						 shoppingListData.setCatagory(RS1.getString(4));
						 shoppingListData.setImage(RS1.getString(5));
					 }
				 }catch (Exception e) {
					// TODO: handle exception
					 System.out.println("Error!!: ");
					 e.printStackTrace();
				}finally {
					try {
						if(RS1 != null) RS1.close();
						}catch (Exception e) {
							// TODO: handle exception
						    e.printStackTrace();
					    }
				    }
				    try {
						if(statement1 != null) statement1.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				 cardData.setShoppingListData(shoppingListData);
			 }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		 return cardData;
		 
	 }
	 
	 
}
