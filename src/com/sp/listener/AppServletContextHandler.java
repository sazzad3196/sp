
package com.sp.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.sp.dao.Database;

public class AppServletContextHandler implements ServletContextListener{
	
	public void contextInitialized(ServletContextEvent event) {
		ServletContext sc=event.getServletContext();
		String databaseDriver = sc.getInitParameter("databaseDriver");
		String databaseName = sc.getInitParameter("databaseName");
		String userName = sc.getInitParameter("userName");
		String password = sc.getInitParameter("password");
		Database db = new Database(databaseDriver, databaseName, userName, password);
		db.open();
		sc.setAttribute("db", db);
		
	}
	
}
