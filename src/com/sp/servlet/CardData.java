package com.sp.servlet;

public class CardData {
	private int id;
	private int userId;
	private int itemId;
	private int quantity;
	private ShoppingListData shoppingListData;
	
	
	public ShoppingListData getShoppingListData() {
		return shoppingListData;
	}
	public void setShoppingListData(ShoppingListData shoppingListData) {
		this.shoppingListData = shoppingListData;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
