package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sp.dao.AdminSignInDao;
import com.sp.dao.Database;

public class AdminSignInServlet extends HttpServlet{
	
	AdminSignInDao adminSignInDao;
	public void init(ServletConfig config) throws ServletException {
		 super.init(config);
		 Database db = (Database)getServletContext().getAttribute("db");
		 adminSignInDao = new AdminSignInDao(db);
		 
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		if(adminSignInDao.adminAuthorization(email, password)) {
			 String val = gson.toJson(true);
			 out.print(val);
		}
		else {
			String val = gson.toJson(false);
			out.print(val);
		}
		out.flush();
	}

}
