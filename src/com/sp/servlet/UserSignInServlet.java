package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.sp.dao.Database;
import com.sp.dao.UserSignInDao;

public class UserSignInServlet extends HttpServlet{
	UserSignInDao userSignInDao;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		userSignInDao = new UserSignInDao(db);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json"); 
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		UserData userData = userSignInDao.userAuthorization(email, password);
		String val = gson.toJson(userData);
		out.print(val);
		out.flush();
	}

}
