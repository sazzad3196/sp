package com.sp.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sp.dao.ShoppingListDao;
import com.sp.dao.Database;

public class ShoppingListServlet extends HttpServlet{
	ShoppingListDao shoppingListDao;
	
	public void init(ServletConfig config) throws ServletException {
		 super.init(config);
		 Database db = (Database)getServletContext().getAttribute("db");
		 shoppingListDao = new ShoppingListDao(db);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("application/json");
		String item = request.getParameter("item");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		if(item.equals("bread")) {
			List<ShoppingListData> ul = shoppingListDao.getAllBread(item);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
		}
		else if(item.equals("fruits")) {
			List<ShoppingListData> ul = shoppingListDao.getAllFruits(item);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
		}
		else if(item.equals("vegetables")) {
			System.out.println("Vegetable: " + item);
			List<ShoppingListData> ul = shoppingListDao.getAllVegetable(item);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
		}
		else if(item.equals("dairy")) {
			System.out.println("Dairy: " + item);
			List<ShoppingListData> ul = shoppingListDao.getAllDairy(item);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
		}
		else if(item.equals("all")) {
			List<ShoppingListData> ul = shoppingListDao.getAllProduct(item);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
		}
		else if(item.equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			if(shoppingListDao.deleteProduct(id)) {
				String val = gson.toJson(true);
				out.print(val);
				out.flush();
			}
			else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
		}
		
		else if(item.equals("deleteCard")) {
			int id = Integer.parseInt(request.getParameter("id"));
			if(shoppingListDao.deleteCardInfo(id)) {
				String val = gson.toJson(true);
				out.print(val);
				out.flush();
			}
			else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
		}
		
		else if(item.equals("deleteOrder")) {
			int id = Integer.parseInt(request.getParameter("id"));
			if(shoppingListDao.deleteOrderInfo(id)) {
				String val = gson.toJson(true);
				out.print(val);
				out.flush();
			}
			else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
		}
		
		else if(item.equals("card")) {
			int userId = Integer.parseInt(request.getParameter("userId"));
			int status = Integer.parseInt(request.getParameter("status"));
			List<CardData> ul = shoppingListDao.getCardInfo(userId, status);
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
			
		}
		
		else if(item.equals("singleCard")) {
			int cardId = Integer.parseInt(request.getParameter("cardId"));
			CardData cardData = shoppingListDao.getSingleCardInfo(cardId, 2);
			String val = gson.toJson(cardData);
			out.print(val);
			out.flush();
			
		}
		
		else if(item.equals("order")) {
			List<OrderData> ul = shoppingListDao.getOrderInfo();
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();
			
		}
		
		else if(item.equals("manageOrder")) {
			List<OrderData> ul = shoppingListDao.getAllOrderInformation();
//			for(OrderData orderData: ul) {
//				UserData userData = new UserData();
//				userData = orderData.getUserData();
//				System.out.println("Name: " + userData.getName());
//				CardData cardData = new CardData();
//				cardData = orderData.getCardData();
//				System.out.println("Quan: " + cardData.getQuantity());
//				ShoppingListData shoppingListData = new ShoppingListData();
//				shoppingListData = cardData.getShoppingListData();
//				System.out.println("Item Name: " + shoppingListData.getName());
//				System.out.println();
//			}
			String val = gson.toJson(ul);
			out.print(val);
			out.flush();	
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		StringBuilder sb = new StringBuilder();
	    BufferedReader reader = request.getReader();
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line).append('\n');
	        }
	    } finally {
	        reader.close();
	    }
	    String result = sb.toString();
	    JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
	    String item = jsonObject.get("item").getAsString();
	    
	    if(item.equals("add")) {
	    	String name = jsonObject.get("name").getAsString();
		    String imageName = jsonObject.get("imageName").getAsString();
		    String catagory = jsonObject.get("catagory").getAsString();
		    int price = jsonObject.get("price").getAsInt();
			if(shoppingListDao.addShoppingList(name, catagory, imageName, price)) {
				String val = gson.toJson(true);
				out.print(val);
				out.flush();
			}
			else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
	    }
	    else if(item.equals("edit")) {
	    	String name = jsonObject.get("name").getAsString();
		    String imageName = jsonObject.get("imageName").getAsString();
		    String catagory = jsonObject.get("catagory").getAsString();
		    int price = jsonObject.get("price").getAsInt();
	    	int id = jsonObject.get("id").getAsInt();
	    	if(shoppingListDao.editShoppingList(id, name, catagory, imageName, price)) {
				String val = gson.toJson(true);
				out.print(val);
				out.flush();
			}
			else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
	    }
	    else if(item.equals("card")) {
	    	int userId = jsonObject.get("userId").getAsInt();
	    	int itemId = jsonObject.get("itemId").getAsInt();
	    	int quantity = jsonObject.get("quantity").getAsInt();
	    	if(this.shoppingListDao.addCardInfo(userId, itemId, quantity)) {
	    		String val = gson.toJson(true);
				out.print(val);
				out.flush();
	    	}
	    	else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
	    }
	    
	    else if(item.equals("order")) {
	    	int cardId = jsonObject.get("cardId").getAsInt();
	    	int userId = jsonObject.get("userId").getAsInt();
	    	String address = jsonObject.get("address").getAsString();
	    	String city = jsonObject.get("city").getAsString();
	    	String date = jsonObject.get("date").getAsString();
	    	if(this.shoppingListDao.setOrderInfo(cardId, userId, address, city, date)) {
	    		String val = gson.toJson(true);
				out.print(val);
				out.flush();
	    	}
	    	else {
				String val = gson.toJson(false);
				out.print(val);
				out.flush();
			}
	    }
	}


}
