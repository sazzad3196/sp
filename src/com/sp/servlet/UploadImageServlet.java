package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;
import com.sp.dao.Database;
import com.sp.dao.UploadImageDao;

public class UploadImageServlet extends HttpServlet{
    UploadImageDao uploadImageDao;
    public void init(ServletConfig config)throws ServletException{
   	 super.init(config);
   	 Database db = (Database)getServletContext().getAttribute("db");
   	 uploadImageDao = new UploadImageDao(db);
    }
    
    public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
   	  response.setContentType("application/json");
   	  PrintWriter out = response.getWriter();
   	  if(ServletFileUpload.isMultipartContent(request)) {
   		    try {
	    		    DiskFileItemFactory factory = new DiskFileItemFactory();
	    		    ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
	    		    FileItemIterator fileItemIterator = servletFileUpload.getItemIterator(request);
	    		    while(fileItemIterator.hasNext()) {
	    		    	 FileItemStream fileItemStream = fileItemIterator.next();
	    		    	 if(!fileItemStream.isFormField()) {
	    		    		   char ch = '.';
	    		    		   String photo = fileItemStream.getName();
	    		    		   int index = photo.lastIndexOf(ch);
	    		    		   String part2 = photo.substring(index);  
	    		 			   String img = System.currentTimeMillis() + part2;
	    		    		   ServletContext context = getServletContext();
	    		    		   String path = context.getRealPath("/"); 
	    		    		   Gson gson = new Gson();
	    		    		   if(uploadImageDao.processFile(path, fileItemStream, img)) {
	    		    			   	 String json = gson.toJson(img);
	    		    			   	 out.print(json);
	    		    			   	 out.flush();
						    	}
						    	else {
						    		  System.out.println("Not Success Fully Upload Image In Project Folder!!!");
						    		  String value = null;
						    		  String json = gson.toJson(value);
		    		    			  out.print(json);
		    		    			  out.flush();
						    	}
	    		    	 }
	    		    }
   		    }catch (Exception e) {
					// TODO: handle exception
   		    	e.printStackTrace();
				}
	    		    
   	  }
    }
}