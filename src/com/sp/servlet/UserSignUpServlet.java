package com.sp.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sp.dao.Database;
import com.sp.dao.UserSignUpDao;

public class UserSignUpServlet extends HttpServlet{
	UserSignUpDao userSignUpDao;
	public void init(ServletConfig config) throws ServletException {
		 super.init(config);
		 Database db = (Database)getServletContext().getAttribute("db");
		 userSignUpDao = new UserSignUpDao(db);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		StringBuilder sb = new StringBuilder();
	    BufferedReader reader = request.getReader();
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line).append('\n');
	        }
	    } finally {
	        reader.close();
	    }
	    
	    String result = sb.toString();
	    JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
	    String name = jsonObject.get("name").getAsString();
	    String email = jsonObject.get("email").getAsString();
	    String password = jsonObject.get("password").getAsString();
	    String phoneNo = jsonObject.get("phoneNo").getAsString();
	    String gender = jsonObject.get("gender").getAsString();
	    String catagory = jsonObject.get("catagory").getAsString();
	    int age = jsonObject.get("age").getAsInt();
	    
	    if(catagory.equals("user")) {
	    	boolean val = userSignUpDao.userSignUp(name, email, password, phoneNo, gender, age);
	    	Gson gson = new Gson();
	    	String value = gson.toJson(val);
	    	out.print(value);
	    	out.flush();
	    }
		
	}
}
