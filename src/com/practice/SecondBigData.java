package com.practice;

import java.util.Scanner;

public class SecondBigData {
	int maxFunction(int[] a, int max) {
		for(int i=0; i<10; i++) {
			if(a[i] > max) {
				max = a[i];
			}
		}
    	return max;
    }
	
	int secondMaxFunction(int[] a, int max, int secondMax) {
		for(int i=0; i<10; i++) {
			if(max > a[i] && a[i] > secondMax) {
				secondMax = a[i];
			}
		}
		return secondMax;
	}
	
	int minFunction(int[] a, int min) {
		for(int i=0; i<10; i++) {
			if(a[i] < min) {
				min = a[i];
			}
		}
    	return min;
    }
	
	int secondMinFunction(int[] a, int min, int secondMin) {
		for(int i=0; i<10; i++) {
			if(min < a[i] && a[i] < secondMin) {
				secondMin = a[i];
			}
		}
		return secondMin;
	}
	
    public static void main(String[] args) {
    	System.out.println("Please Input: ");
    	SecondBigData secondBigData = new SecondBigData();
    	Scanner myObj = new Scanner(System.in);
    	int[] a = new int[10];
    	for(int i=0; i<10; i++) {
    		a[i] = myObj.nextInt();
    	}
    	
    	int max = a[0];
    	int secondMax = a[0];
    	max = secondBigData.maxFunction(a, max);
    	secondMax = secondBigData.secondMaxFunction(a, max, secondMax);
    	System.out.println("SecondMax Value: " + secondMax);
    	
    	int min = a[0];
    	int secondMin = a[0];
    	min = secondBigData.minFunction(a, min);
    	secondMin = secondBigData.secondMinFunction(a, min, secondMin);
    	System.out.println("SecondMin Value: " + secondMin);
    }
    
    
}


